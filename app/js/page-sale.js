$(function() {

    $(".col").on("click", ".col-header", function(e) {
        var block = $(e.delegateTarget);

        if(block.toggleClass("col-hidden").hasClass("col-hidden")) {
            block.find(".toggle").html('Развернуть <i class="fa fa-chevron-down"></i>');

        } else {
            block.find(".toggle").html('Свернуть <i class="fa fa-chevron-up"></i>');
        }
    }).each(function() {
        if($(this).hasClass("col-hidden")) {
            $(this).find(".toggle").html('Развернуть <i class="fa fa-chevron-down"></i>');

        } else {
            $(this).find(".toggle").html('Свернуть <i class="fa fa-chevron-up"></i>');
        }
    });

    $(function() {

        $("li.dropdown").click(function() {
            $(this).toggleClass("open");
        })

    })

});
