$(function() {
    $(".photos-slider").owlCarousel({
        items: 3,
        nav: true,
        stagePadding: 0,
        margin: 10,
        navText: ["<i class='fa fa-chevron-left'> </i>", "<i class='fa fa-chevron-right'> </i>"],
    })

    $(".col").on("click", ".col-header", function(e) {
        var block = $(e.delegateTarget);

        if(block.toggleClass("col-hidden").hasClass("col-hidden")) {
            block.find(".toggle").html('Развернуть <i class="fa fa-chevron-down"></i>');

        } else {
            block.find(".toggle").html('Свернуть <i class="fa fa-chevron-up"></i>');
        }
    }).each(function() {
        if($(this).hasClass("col-hidden")) {
            $(this).find(".toggle").html('Развернуть <i class="fa fa-chevron-down"></i>');

        } else {
            $(this).find(".toggle").html('Свернуть <i class="fa fa-chevron-up"></i>');
        }
    });
});

