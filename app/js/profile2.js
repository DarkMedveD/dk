$(function() {

    $("#content .tabs").hide();
    $("#tabs li:first").attr("id","current");
    $("#content div:first").fadeIn();

    $('#tabs a').click(function(e) {
        e.preventDefault();
        $("#content .tabs").hide();
        $("#tabs li").attr("id","");
        $(this).parent().attr("id","current");
        $('#' + $(this).attr('title')).fadeIn();

        if($(this).hasClass('tab1')){
            $('h2 span').text('Реквизиты и контакты');
        }
        if($(this).hasClass('tab2')){
            $('h2 span').text('Документы и шаблоны');
        }
        if($(this).hasClass('tab3')){
            $('h2 span').text('Заказы компании');
        }
        if($(this).hasClass('tab4')){
            $('h2 span').text('Настройка уведомлений');
        }
        if($(this).hasClass('tab5')){
            $('h2 span').text('Настройки');
        }



    });

});



