$(function() {

    $(".grid-row").each(function() {

        $(this).masonry({
            itemSelector: ".grid-item",
            columnWidth: 260,
            gutter: 18
        })
    })


    $("#content .tabs").hide();
    $("#tabs li:first").attr("id","current");
    $("#content div:first").fadeIn();

    $('#tabs a').click(function(e) {
        e.preventDefault();
        $("#content .tabs").hide();
        $("#tabs li").attr("id","");
        $(this).parent().attr("id","current");
        $('#' + $(this).attr('title')).fadeIn();

        if($(this).hasClass('tab1')){
            $('h2 span').text('Акции и скидки');
        }
        if($(this).hasClass('tab2')){
            $('h2 span').text('Дисконтная карта');
        }
        if($(this).hasClass('tab3')){
            $('h2 span').text('Мои заказы');
        }
        if($(this).hasClass('tab4')){
            $('h2 span').text('Лист пожеланий');
        }
        if($(this).hasClass('tab5')){
            $('h2 span').text('Настройки');
        }



    });

    $(".col").on("click", ".col-header", function(e) {
        var block = $(e.delegateTarget);

        if(block.toggleClass("col-hidden").hasClass("col-hidden")) {
            block.find(".toggle").html('Развернуть <i class="fa fa-chevron-down"></i>');

        } else {
            block.find(".toggle").html('Свернуть <i class="fa fa-chevron-up"></i>');
        }
    }).each(function() {
        if($(this).hasClass("col-hidden")) {
            $(this).find(".toggle").html('Развернуть <i class="fa fa-chevron-down"></i>');

        } else {
            $(this).find(".toggle").html('Свернуть <i class="fa fa-chevron-up"></i>');
        }
    });
});



