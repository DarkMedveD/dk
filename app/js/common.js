$(function() {
    initGoodsCarousel();
    initGridRow();
    initSelect();
});


function initGoodsCarousel() {
    $(".goods-row").each(function() {

        var caroulsel = $(this).find(".goods-carousel").owlCarousel({
            items: 4
        });

        $(this).find(".arrow-left").click(function() {
            caroulsel.trigger("prev.owl.carousel");
        })

        $(this).find(".arrow-right").click(function() {
            caroulsel.trigger("next.owl.carousel");
        })

    });

}

function initGridRow() {
    $(".grid-row").each(function() {

        $(this).masonry({
            itemSelector: ".grid-item",
            columnWidth: 288,
            gutter: 18
        })
    })
}

function initSelect() {
    $('select').select2({
        minimumResultsForSearch: Infinity
    });
}