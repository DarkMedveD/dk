var filtersTop;

$(function() {
    $(".filter h4").click(function(){
       $(this).parent().toggleClass("open");
    });

    range = $("#input-range");

    noUiSlider.create(range[0], {
        start: [ range.data("min"), range.data("max") ],
        connect: true,
        step: 1,
        tooltips: [ wNumb({ decimals: 0, postfix: ' ₽' }), wNumb({ decimals: 0, postfix: ' ₽' }) ],
        range: {
            'min': range.data("min"),
            'max': range.data("max")
        }
    });

    range[0].noUiSlider.on('update', function( values, handle ) {
        $("#filter-range-min").val(parseInt(values[0]) + ' ₽');
        $("#filter-range-max").val(parseInt(values[1]) + ' ₽');
    });

    $("#filter-range-min").change(function() {
        range[0].noUiSlider.set([this.value, null]);
    });

    $("#filter-range-max").change(function() {
        range[0].noUiSlider.set([null, this.value]);
    });

    filtersTop = $(".filters-wrapper").offset().top;

    $(window).scroll(function() {
        $(".filters-wrapper").toggleClass("fixed",$(window).scrollTop()+30 > filtersTop);
    });

});