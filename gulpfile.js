
var gulp = require("gulp");
var scss = require("gulp-scss");
var concat = require("gulp-concat");
var uglify = require("gulp-uglifyjs");
var browserSync= require("browser-sync");
var autoprefixer = require("gulp-autoprefixer");
var del = require("del");
var bower = require("gulp-bower");
var includeTag = require('gulp-include-tag');
var imageMin = require("gulp-imagemin");
var pngquant = require("imagemin-pngquant");


gulp.task("styles", [], function() {
	return gulp.src(["app/scss/**/*.scss"])
		.pipe(scss())
		.pipe(autoprefixer())
		.pipe(gulp.dest("app/css"))
		.pipe(browserSync.reload({stream: true}))
});

gulp.task("scripts", function() {
	return gulp.src([
		'app/libs/jquery/dist/jquery.min.js',
		'app/libs/bootstrap/dist/js/bootstrap.min.js',
		'app/libs/owl.carousel/dist/owl.carousel.js',
		'app/libs/masonry/dist/masonry.pkgd.min.js',
		'app/libs/select2/dist/js/select2.min.js',
        'app/libs/nouislider/distribute/nouislider.min.js',
        'app/libs/wnumb/wNumb.js',
	])
		.pipe(concat('libs.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest("app/js"));
});

gulp.task("images", function() {

	return gulp.src("app/img/**/*")
		.pipe(imageMin({
			interlaced: true,
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			une: [pngquant]
		}))
		.pipe(gulp.dest("dist/img"))

});

gulp.task('fonts', function() {
    return gulp.src([
        'app/libs/components-font-awesome/fonts/**.*',
        'app/libs/open-sans-fontface/fonts/**/*',
        'app/libs/bootstrap/dist/fonts/**/*'
    ])
        .pipe(gulp.dest('app/fonts'));
});

gulp.task("styles-libs", function() {
	return gulp.src([
		'app/libs/normalize-css/normalize.css',
		'app/libs/components-font-awesome/css/font-awesome.css',
		'app/libs/open-sans-fontface/open-sans.scss',
		'app/libs/bootstrap/dist/css/bootstrap.min.css',
		'app/libs/owl.carousel/dist/assets/owl.carousel.css',
		'app/libs/select2/dist/css/select2.min.css',
		'app/libs/nouislider/distribute/nouislider.min.css',
	])
		.pipe(scss())
		.pipe(concat("libs.min.css"))
		.pipe(gulp.dest("app/css"))
});

gulp.task("clean", function() {
	return del.sync("dist");
});

gulp.task("build", ['clean','images',"styles-libs", 'fonts','styles', 'scripts'], function() {

	var bulidCss = gulp.src('app/css/*')
	.pipe(gulp.dest("dist/css"));

	var buildFonts = gulp.src('app/fonts/**/*')
	.pipe(gulp.dest('dist/fonts'));

	var buildJs = gulp.src('app/js/**/*')
	.pipe(gulp.dest("dist/js"));

	var buldHtml = gulp.src("app/*.html")
	.pipe(gulp.dest("dist"));

});


gulp.task("html", function() {
	return gulp.src([
		"app/html/*.html",
		"!app/html/_**.html"
	])
		.pipe(includeTag())
		.pipe(gulp.dest("app"))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('bower', function() {
    return bower({ cmd: 'update'})
        .pipe(gulp.dest('vendor/'))
});


gulp.task("browser-sync", function() {
	browserSync({
		server: {
			baseDir: "app"
		}
	})
});

gulp.task("watch", ['html', 'browser-sync', 'styles'], function() {
	gulp.watch('app/scss/**/*.scss', ['styles']);
	gulp.watch("app/html/*.html", ['html']);
	gulp.watch("app/js/**/*.js", browserSync.reload);
});